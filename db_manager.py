#!/usr/bin/env python3
"""
DatabaseManager class for managing SQLite database interactions.
Handles connection pooling, executing queries, and integrates with logging.
"""

import sqlite3
import logging
import os
from queue import Queue
from threading import Lock
from dotenv import load_dotenv
from log_manager import LogManager

class DatabaseManager:
    """
    Manages database interactions using SQLite. Implements connection pooling and
    executes SQL queries. Designed as a singleton to ensure a single instance across 
    the application.
    """
    _singleton_instance = None
    _singleton_lock = Lock()

    def __new__(cls):
        """
        Ensures that only one instance of the DatabaseManager is created (singleton pattern).
        """
        if cls._singleton_instance is None:
            cls._singleton_instance = super(DatabaseManager, cls).__new__(cls)
            load_dotenv()
            cls._singleton_instance.db_path = os.getenv("DB_PATH")
            cls._singleton_instance.connection = sqlite3.connect(
                cls._singleton_instance.db_path, check_same_thread=False
            )
            cls._singleton_instance.cursor = cls._singleton_instance.connection.cursor()
            cls._singleton_instance.log_manager = LogManager(os.getenv("LOG_FILE"))
        return cls._singleton_instance

    def __init__(self):
        """
        Initializes the DatabaseManager instance. Loads environment variables to set up 
        database path and logging. It initializes the connection pool for managing database 
        connections. Ensures that initialization occurs only once for the singleton instance.
        """
        if not hasattr(self, '_initialized'):
            load_dotenv()
            self.db_path = os.getenv("DB_PATH")
            self.log_file = os.getenv("LOG_FILE")
            self.log_manager = LogManager(self.log_file)
            self.pool_size = 5
            self.connection_pool = Queue(self.pool_size)
            self._initialize_connection_pool()
            self._initialized = True

    def _initialize_connection_pool(self):
        """
        Initializes the connection pool for the database. Creates connections up to the 
        specified pool size and adds them to the pool.
        """
        for _ in range(self.pool_size):
            connection = sqlite3.connect(self.db_path, check_same_thread=False)
            self.log_manager.log(logging.INFO, "Database connection opened.")
            self.connection_pool.put(connection)

    def _get_connection(self):
        """
        Retrieves a database connection from the connection pool.
        Returns a connection object.
        """
        self.log_manager.log(logging.INFO, "Database connection retrieved from pool.")
        return self.connection_pool.get()

    def _release_connection(self, connection):
        """
        Releases a used database connection back to the connection pool.
        """
        self.log_manager.log(logging.INFO, "Database connection released to pool.")
        self.connection_pool.put(connection)

    def initialize_db(self):
        """
        Initializes the database by creating necessary tables if they don't exist.
        """
        if not self._is_db_initialized():
            connection = self._get_connection()
            try:
                with connection:
                    self._create_tables(connection)
            except sqlite3.DatabaseError as e:
                self.log_manager.log(logging.ERROR, f'Database initialization error: {e}')
            finally:
                self._release_connection(connection)

    def _is_db_initialized(self):
        """
        Checks if the database has been initialized by looking for a specific table.
        """
        check_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='users';"
        connection = self._get_connection()
        try:
            with connection:
                cursor = connection.cursor()
                cursor.execute(check_query)
                return cursor.fetchone() is not None
        finally:
            self._release_connection(connection)
        return False

    def run_query(self, query, params=None, query_type='fetch', commit=False):
        """
        Executes a SQL query with optional parameters.
        - 'query_type' can be 'fetch', 'modify', or 'insert'.
        - 'commit' indicates whether to commit the transaction.
        """
        connection = self._get_connection()
        results = None
        row_id = None

        try:
            cursor = connection.cursor()
            cursor.execute(query, params or ())
            if query_type == 'fetch':
                results = cursor.fetchall()
            if query_type == 'insert':
                row_id = cursor.lastrowid
            if commit:
                connection.commit()
        except sqlite3.Error as e:
            self.log_manager.log(logging.ERROR, f"Database error in run_query: {e}")
        finally:
            self._release_connection(connection)

        return results, row_id

    def _create_tables(self, connection):
        """
        Creates necessary tables in the database based on predefined SQL commands.
        This method is called during database initialization.
        """
        cursor = connection.cursor()
        # List of SQL commands to create necessary tables and indices
        table_commands = [
            '''CREATE TABLE IF NOT EXISTS chatgpt_interactions (
                id INTEGER PRIMARY KEY,
                username TEXT NOT NULL,
                system_message TEXT NOT NULL,
                user_message TEXT NOT NULL,
                response TEXT NOT NULL,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )''',
            '''CREATE INDEX IF NOT EXISTS idx_chatgpt_interactions_timestamp_username
            ON chatgpt_interactions(timestamp, username)''',
            '''CREATE TABLE IF NOT EXISTS greetings (
                username TEXT PRIMARY KEY,
                last_greeted DATETIME NOT NULL
            )''',
            '''CREATE TABLE IF NOT EXISTS user_feedback (
                id INTEGER PRIMARY KEY,
                interaction_id INTEGER REFERENCES chatgpt_interactions(id) ON DELETE CASCADE,
                username TEXT NOT NULL,
                feedback TEXT NOT NULL,
                type TEXT CHECK(type IN ('feedback', 'complaint')),
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )''',
            '''CREATE INDEX IF NOT EXISTS idx_user_feedback_timestamp_username_interaction_id
            ON user_feedback(timestamp, username, interaction_id)''',
            '''CREATE TABLE IF NOT EXISTS daily_usage (
                username TEXT PRIMARY KEY,
                casual_conversations_count INT NOT NULL DEFAULT 0,
                last_updated DATE NOT NULL DEFAULT CURRENT_DATE
            )''',
            '''CREATE TABLE IF NOT EXISTS user_attacks (
                id INTEGER PRIMARY KEY,
                interaction_id INTEGER REFERENCES chatgpt_interactions(id) ON DELETE CASCADE,
                username TEXT NOT NULL,
                threat_category TEXT NOT NULL,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )''',
            '''CREATE INDEX IF NOT EXISTS idx_user_attacks_timestamp_username_interaction_id
            ON user_attacks(timestamp, username, interaction_id)''',
            '''CREATE TABLE IF NOT EXISTS prompts (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                persona TEXT UNIQUE,
                last_used DATETIME
            )''',
            '''
            CREATE TABLE IF NOT EXISTS sent_messages (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                timestamp TEXT
            )
            ''',
            '''
            CREATE TABLE IF NOT EXISTS threat_intel (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                ip_address TEXT NOT NULL,
                country_code TEXT NOT NULL,
                geopolitical_risk TEXT,
                cyber_activity_risk TEXT,
                economic_risk TEXT,
                political_stability_risk TEXT,
                overall_threat_score INTEGER,
                specific_threat_types TEXT,
                recent_trends TEXT,
                regional_conflicts TEXT,
                legal_cyber_stance TEXT,
                tech_infrastructure TEXT,
                recommendations TEXT,
                geolocation_data TEXT,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )
            ''',
            '''
            CREATE TABLE IF NOT EXISTS users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT UNIQUE NOT NULL,
                email TEXT UNIQUE,
                created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
                last_login DATETIME,
                status TEXT CHECK(status IN ('active', 'inactive', 'suspended'))
            )
            ''',
            '''
            CREATE TABLE IF NOT EXISTS user_threads (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT NOT NULL,
                thread_id TEXT NOT NULL,
                last_updated DATETIME DEFAULT CURRENT_TIMESTAMP,
                UNIQUE(username)
            )
            '''
        ]

        for command in table_commands:
            cursor.execute(command)
            self.log_manager.log(
                logging.INFO, f"Database structure updated with command: {command}"
            )

    def __del__(self):
        """
        Closes all database connections in the connection pool upon the destruction of the instance.
        """
        while not self.connection_pool.empty():
            connection = self.connection_pool.get()
            if connection:
                connection.close()
                self.log_manager.log(logging.INFO, "Database connection closed.")


# Example usage of the DatabaseManager class
if __name__ == "__main__":
    db_manager = DatabaseManager()
    db_manager.initialize_db()

    # Example query usage
    # Fetch all users
    QUERY = 'SELECT * FROM users'
    users, _ = db_manager.run_query(QUERY)
    print("Users:", users)

    # Insert a new user
    INSERT_QUERY = 'INSERT INTO users (username, email) VALUES (?, ?)'
    _, new_user_id = db_manager.run_query(
        INSERT_QUERY,
        ('new_user', 'new_user@example.com'),
        query_type='insert', commit=True
    )
    print("New User ID:", new_user_id)

    # Modify an existing user's last login time
    UPDATE_QUERY = 'UPDATE users SET last_login = CURRENT_TIMESTAMP WHERE username = ?'
    db_manager.run_query(UPDATE_QUERY, ('existing_user',), query_type='modify', commit=True)
    print("User 'existing_user' last login updated.")
