#!/usr/bin/env python3
"""
A script to manage interactions with the OpenAI ChatGPT API, encompassing query processing,
response handling, and logging. Integrates database management for storing API interactions
and utilizes environmental variables for configuration.
"""

import os
import json
import logging
import asyncio
import sqlite3
import openai
from dotenv import load_dotenv
from db_manager import DatabaseManager
from log_manager import LogManager

class ChatGPTInteractionManager:
    """
    Manages interactions with the OpenAI ChatGPT API, handling queries and processing responses.
    Supports various message interaction types including user-only, system and user, 
    and potential future assistant features.
    """

    def __init__(self):
        """
        Initializes the ChatGPTInteractionManager. Loads environment variables, sets up 
        the OpenAI API key, and initializes the database and log managers.
        Raises ValueError if the OpenAI API key is not found in the environment variables.
        """
        # Load environment variables
        load_dotenv()
        log_file = os.getenv("LOG_FILE")
        self.api_key = os.getenv("OPENAI_API_KEY")
        if not self.api_key:
            raise ValueError("OpenAI API key not found in environment variables.")

        openai.api_key = self.api_key
        self.db_manager = DatabaseManager._singleton_instance
        self.db_manager.initialize_db()
        self.log_manager = LogManager(log_file)

    async def record_api_interaction(self, username, user_message, response):
        """
        Records an interaction with the OpenAI API in the database. Logs the system message,
        user message, and response from the ChatGPT API.
        """
        self.log_manager.log(logging.INFO, "Logging ChatGPT interaction to database.")
        query = '''
            INSERT INTO chatgpt_interactions (username, system_message, user_message, response)
            VALUES (?, ?, ?, ?)
        '''
        try:
            self.db_manager.run_query(
                query,
                (username, system_message, user_message, response),
                commit=True)
            self.log_manager.log(logging.INFO, "Logged ChatGPT interaction successfully.")
        except sqlite3.Error as error:
            self.log_manager.log(logging.ERROR, f"Database error during logging: {error}")

    async def query_with_user_message(self, user_message, model="text-davinci-003", tokens=150, temperature=0.75):
        """
        Sends a query to ChatGPT with only a user message. 
        """
        self.log_manager.log(logging.INFO, "Querying ChatGPT.")
        try:
            response = openai.Completion.create(
                model=model,
                prompt=user_message,
                max_tokens=tokens,
                temperature=temperature
            )
            self.log_manager.log(logging.INFO, "Received query response from ChatGPT.")
            response_text = response.choices[0].text.strip()
            # Record the API interaction
            await self.record_api_interaction("user", user_message, response_text)
            return {"response": response_text}
        except openai.OpenAIError as error:
            self.log_manager.log(logging.ERROR, f"OpenAI API error: {error}")
            return {"error": str(error)}


# Example usage of the ChatGPTInteractionManager class
if __name__ == "__main__":
    chatgpt_manager = ChatGPTInteractionManager()

    # Example 1: Combined system and user message
    async def send_combined_query():
        """
        Sends a combined system and user message query to ChatGPT and prints the response.
        """
        username = "example_user"
        system_message = "You are a helpful assistant."
        user_message = "What's the weather forecast for Paris today?"
        response_json = await chatgpt_manager.send_combined_query(
            username, system_message, user_message
        )
        print("Combined Query Response:", response_json)

    # Example 2: User-only message
    async def send_user_message_query():
        """
        Sends only a user message query to ChatGPT and prints the response.
        """
        user_message = "Tell me a joke about cats."
        response_json = await chatgpt_manager.query_with_user_message(user_message)
        print("User Message Query Response:", response_json)

    # Execute the asynchronous functions
    asyncio.run(send_combined_query())
    asyncio.run(send_user_message_query())
