#!/usr/bin/env python3
"""
Defines the ReminderBot class to automate sending persona-based reminders via Slack using ChatGPT.
"""

import os
import sys
import time
import json
import argparse
import random
import requests
from dotenv import load_dotenv
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from chatgpt_connector import ChatGPTInteractionManager
from db_manager import DatabaseManager
from log_manager import LogManager

class ReminderBot:
    """
    Automates sending reminders via Slack. Uses ChatGPT for generating messages
    based on different personas and tracks message history in a database.
    """

    def __init__(self, reminder_topic, more_info_link=None):
        """
        Initializes the bot with reminder topic, Slack configuration, and database setup.
        """
        load_dotenv()
        self.reminder_topic = reminder_topic
        self.more_info_link = more_info_link
        self.slack_client = WebClient(token=os.getenv("SLACK_TOKEN"))
        self.channel_id = os.getenv("CHANNEL_ID")
        self.giphy_api_key = os.getenv("GIPHY_API_KEY")
        self.db_manager = DatabaseManager._singleton_instance
        self.chatgpt_api = ChatGPTInteractionManager()
        self.log_manager = LogManager('reminder_bot.log')
        self.db_manager.initialize_db()

    def populate_db(self):
        """
        Populates the database with personas read from a file, updating last used timestamps.
        """
        with open("personas.txt", "r", encoding="utf-8") as f:
            personas = {line.strip() for line in f if line.strip()}

        for persona in personas:
            self.db_manager.run_query(
                "INSERT OR IGNORE INTO prompts (persona, last_used) "
                "VALUES (?, datetime('now', '-49 hours'))", 
                (persona,), commit=True
            )

        existing_personas = set(
            row[0] for row in self.db_manager.run_query(
                "SELECT persona FROM prompts"
            )
        )

        for persona in existing_personas - personas:
            self.db_manager.run_query(
                "DELETE FROM prompts WHERE persona = ?", 
                (persona,), commit=True
            )

    def get_random_persona(self):
        """
        Selects a random persona from the database that hasn't been used recently.
        """
        row = self.db_manager.run_query(
            "SELECT persona FROM prompts "
            "WHERE last_used IS NULL OR last_used < datetime('now', '-48 hours') "
            "ORDER BY RANDOM() LIMIT 1"
        )
        if not row:
            print("No available personas. Exiting.")
            sys.exit(1)
        persona = row[0][0]
        self.update_last_used(persona)
        return persona

    def update_last_used(self, persona):
        """
        Updates the 'last used' timestamp of a persona in the database.
        """
        self.db_manager.run_query(
            "UPDATE prompts SET last_used = datetime('now') WHERE persona = ?",
            (persona,), commit=True
        )

    def generate_persona_message(self):
        """
        Generates a creative message for a randomly selected persona using ChatGPT.
        """
        username = "soc-bot"
        persona = self.get_random_persona()
        system_message = f"Craft a message as {persona} for reminder topic '{self.reminder_topic}'."
        user_message = f"Generate a reminder message as {persona}."

        response_json = self.chatgpt_api.send_combined_query(
            username, system_message, user_message
        )
        response = json.loads(response_json)
        return f"{persona}: {response['response']}"

    def store_message_timestamp(self, timestamp):
        """
        Stores the timestamp of a sent message in the database.
        """
        self.db_manager.run_query(
            "INSERT INTO sent_messages (timestamp) VALUES (?)", 
            (timestamp,), commit=True
        )

    def cleanup_old_messages(self, time_limit):
        """
        Cleans up recent messages in Slack and the database based on a specified time limit.
        """
        cutoff_time = time.time() - time_limit
        recent_timestamps = self.db_manager.run_query(
            "SELECT timestamp FROM sent_messages WHERE timestamp > ?",
            (cutoff_time,)
        )

        for ts in recent_timestamps:
            try:
                self.slack_client.chat_delete(channel=self.channel_id, ts=ts[0])
                self.db_manager.run_query(
                    "DELETE FROM sent_messages WHERE timestamp = ?",
                    (ts[0],), commit=True
                )
            except SlackApiError as slack_error:
                print(f"Error during cleanup of recent messages: {slack_error}")

    def get_giphy_url(self, persona):
        """
        Fetches a Giphy URL for a given persona.
        """
        base_url = "https://api.giphy.com/v1/gifs/search"
        search_params = f"?api_key={self.giphy_api_key}&q={persona}&rating=g&limit=5"
        response = requests.get(f"{base_url}{search_params}", timeout=30)
        response.raise_for_status()
        data = response.json()
        return (
            random.choice(data['data'])['images']['fixed_height_small']['url']
            if data['data'] else None
        )

    def send_to_slack(self, persona, message):
        """
        Sends a crafted message to Slack, including an optional Giphy image and 'More Info' link.
        """
        try:
            giphy_url = self.get_giphy_url(persona)
            blocks = [
                {"type": "divider"},
                {
                    "type": "image", 
                    "image_url": giphy_url, 
                    "alt_text": persona
                },
                {
                    "type": "section", 
                    "text": {
                        "type": "mrkdwn", 
                        "text": f"*{persona}*: _{message}_"
                    }
                }
            ]

            if self.more_info_link:
                blocks += [
                    {
                        "type": "actions", 
                        "elements": [
                            {
                                "type": "button", 
                                "text": {
                                    "type": "plain_text", 
                                    "text": "More Info"
                                },
                                "url": self.more_info_link, 
                                "action_id": "button_more_info"
                            }
                        ]
                    },
                    {"type": "divider"}
                ]

            response = self.slack_client.chat_postMessage(channel=self.channel_id, blocks=blocks)
            self.store_message_timestamp(response['ts'])

        except SlackApiError as slack_error:
            print(f"Error sending message to Slack: {slack_error}")

    def run(self):
        """
        Runs the main process of the bot: populating the database, generating messages,
        cleaning up old messages, and sending new ones to Slack.
        """
        self.populate_db()
        creative_message = self.generate_persona_message()
        persona, message = creative_message.split(": ", 1)
        self.cleanup_old_messages(1200)
        self.send_to_slack(persona, message)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run the ReminderBot.')
    parser.add_argument('--topic', help='The reminder topic to be used in prompts.')
    parser.add_argument('--more-info-link', help='URL for the More Info button.')
    parser.add_argument('--cleanup-only', action='store_true', help='Run in cleanup-only mode.')
    args = parser.parse_args()

    bot = ReminderBot(args.topic, args.more_info_link)
    if args.cleanup_only:
        bot.cleanup_old_messages(1200)  # 20 minutes
    else:
        bot.run()
