#!/usr/bin/env python3
"""
FastAPI application serving as an interface with a Slackbot. Processes Slack requests,
evaluates for specific commands, and uses ChatGPT API for responses. Implements rate limiting, 
greeting checks, command processing, and security evaluations. Acts as an intermediary 
to handle and format responses to the Slackbot. Also, handles document uploads and processes 
direct messages sent to the Slackbot, forwarding them to the OpenAI Assistant.
"""

from pydantic import BaseModel
from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import JSONResponse
from db_manager import DatabaseManager
from slack_utils import SlackbotHandler

# Initialize the DatabaseManager to ensure its singleton instance is created
db_manager = DatabaseManager()

# Initialize the FastAPI application and utility objects
app = FastAPI()
slackbot_handler = SlackbotHandler()

class SlackRequest(BaseModel):
    """
    Defines the expected data structure for incoming Slack requests, 
    capturing user details and message content.
    """
    user: str  # The username of the Slack user sending the request
    text: str  # The full text of the user's message

@app.get("/")
async def read_root():
    """
    Provides a basic response at the root endpoint for service status checks.
    """
    return {"Hello": "World"}

@app.post("/slack-message")
async def handle_slack_message(request: Request):
    """
    Main endpoint for handling Slack interactions. Processes the request using 
    Slackbot utilities, applies rate limiting, conducts security assessments, 
    and generates responses.
    """
    try:
        data = await request.json()
        slack_request = SlackRequest(**data)
        response = await slackbot_handler.process_user_input(slack_request)
        return JSONResponse(content=response)
    except Exception as error:
        raise HTTPException(status_code=500, detail=str(error)) from error
