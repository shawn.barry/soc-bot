# soc-bot

## Overview

soc-bot is a Slackbot designed to send automated reminders with creativity through various personas using OpenAI's GPT-3 API. It also integrates with Giphy for dynamic images and provides an optional "More Info" link for added context. Additionally, it evaluates incoming messages for potential security threats and responds accordingly.

## Features

- SQLite database for prompt storage and tracking identified threats.
- Slack SDK for Python integration.
- Message generation using GPT-3.
- Giphy API integration for relevant images.
- Optional "More Info" button linking to external resources.
- Scheduled reminders.
- Threat evaluation using ChatGPT.

## Prerequisites

- Python 3.x
- Slack SDK for Python
- OpenAI Python package
- Requests
- Python-dotenv
- A Slack bot token, GPT-3 API key, and Giphy API key.

## Setup

### Environment

1. Clone the repository.

    ```bash
    git clone https://gitlab.com/shawn.barry/soc-bot.git
    ```

2. Create a virtual environment:

    ```bash
    python3 -m venv soc-bot-venv
    ```

3. Activate the virtual environment:

    ```bash
    source soc-bot-venv/bin/activate
    ```

4. Install dependencies.

    ```bash
    pip install -r requirements.txt
    ```

### Configuration

1. Rename `.env.example` to `.env` and modify it with your actual credentials.

    ```bash
    cp .env.example .env
    nano .env
    ```

2. Set environment variables in a `.env` file.

    ```env
    SLACK_TOKEN=your_slack_token_here
    GPT_API_KEY=your_gpt_api_key_here
    GIPHY_API_KEY=your_giphy_api_key_here
    CHANNEL_ID=your_slack_channel_id_here
    ```

3. The SQLite database will be automatically initialized and populated on each run. Ensure `messages.txt` is in the project directory for proper initialization.

    **Note**: If you remove a persona from `messages.txt`, manually delete the existing SQLite database for changes to take effect.

4. Run the bot.

    ```bash
    python soc-bot.py --topic "Your Topic Here" [--link "http://example.com"]
    ```

## Usage

### Scheduled Reminders

To send reminders at scheduled intervals, execute `scheduled_reminders.py`. This script reads the `messages.txt` file to fetch the list of reminders and sends them accordingly.

```bash
python scheduled_reminders.py --topic "Water the plants" [--link "http://example.com"]
```

### Slackbot Listener

The `slack_message_receiver.py` script acts as a real-time listener for incoming messages on Slack channels where the bot is present. It utilizes the Slack RTM (Real-Time Messaging) API to continuously monitor and fetch new messages. 

Upon receiving a message, it performs the following tasks:
- Analyzes the message content to determine if it falls into a predefined category or contains specific keywords.
- If the message is identified as a potential security threat, it uses the `UserInputAnalyzer` class to assess the threat and generate a creative response.
- Sends the generated response back to the Slack channel.

To run the Slackbot listener, execute the following command:

```bash
python slack_message_receiver.py
```

### Threat Evaluation

The `user_input_analyzer.py` script defines a `UserInputAnalyzer` class responsible for assessing potential security threats in messages and responding creatively. This class interacts with ChatGPT to evaluate user input, classifying potential security threats and generating appropriate responses. Additionally, it maintains a SQLite database to track identified threats and responses.

#### Methods:

- **analyze_user_input(user_input, username)**:
  - **Purpose**: Evaluates the user input for potential security threats and returns an evaluation result.
  - **Parameters**:
    - `user_input`: The content of the message to evaluate.
    - `username`: The username of the sender of the message.
  - **Returns**: A JSON string containing the evaluation result, which includes information about potential threats, ChatGPT responses, and other categorized intents.
  - **Usage**: To use the UserInputAnalyzer class for evaluating messages, the following code snippet can be used
    ```python
    security_evaluator = UserInputAnalyzer()
    INPUT = "INSERT INTO users (username, password) VALUES ('admin', 'admin');"
    USER = "example_user"
    analysis_result = security_evaluator.analyze_user_input(USER, INPUT)
    print(analysis_result)
    ```

#### Threat Evaluation Workflow:

The `analyze_user_input` method goes through the following workflow:
1. **Initialize Response**: Initializes a default response structure.
2. **Parse ChatGPT Response**: Queries ChatGPT and parses the response to assess the input for potential security threats.
3. **Process Parsed Response**: Processes the parsed response and updates the main evaluation response accordingly, identifying threats or other intents.

#### Threat Evaluation Details:

Threat evaluation occurs within the `analyze_user_input` method. Specifically, after parsing the response from ChatGPT, the method processes the parsed response to determine if a security threat exists and then categorizes it accordingly. The evaluation result includes details such as threat category, reason, intent, reverse shell interaction, and context of the message.

```json
{
  "status": "success",
  "message": null,
  "data": {
    "is_malicious": true,
    "threat_category": "SQL Injection",
    "reason": "Detected an attempt to manipulate SQL queries.",
    "chatgpt_response": "Nice try, but I can't let you do that!",
    "is_feedback": false,
    "is_complaint": false,
    "is_question": false,
    "is_attack": true,
    "context": "casual_conversation",
    "intent": "attack"
  },
  "error": null
}

```

## Automated Task Scheduling

To ensure that the bot sends reminders and listens for incoming messages efficiently, you can utilize either **Systemd Timers** or **Cron Jobs** for task scheduling.

### Systemd Timers

Systemd timers provide a flexible and integrated approach for scheduling tasks. Here is how you can set them up:

1. **Create a Service File** (`scheduled_reminders.service`):
   ```ini
   [Unit]
   Description=Send scheduled reminders

   [Service]
   ExecStart=/path/to/python scheduled_reminders.py
   ```

2. **Create a Timer File** (`scheduled_reminders.timer`):
   ```ini
   [Unit]
   Description=Timer for scheduled reminders

   [Timer]
   OnCalendar=*-*-* 12,00:00:00

   [Install]
   WantedBy=timers.target
   ```

3. **Activate the Timer**:
   ```bash
   systemctl start scheduled_reminders.timer
   systemctl enable scheduled_reminders.timer
   ```

4. **Monitor the Timer**:
   ```bash
   systemctl list-timers
   ```

### Systemd Service

To ensure the Slackbot listener starts at boot and restarts on failure:

1. **Create a Service File** (`slack_message_receiver.service`):
   ```ini
   [Unit]
   Description=Slackbot Listener
   After=network.target

   [Service]
   ExecStart=/path/to/python slack_message_receiver.py
   Restart=on-failure

   [Install]
   WantedBy=default.target
   ```

2. **Enable and Start the Service**:
   ```bash
   systemctl enable slack_message_receiver.service
   systemctl start slack_message_receiver.service
   ```

### Cron Jobs

For simpler task scheduling, you can also use Cron Jobs:

```bash
# Set the PATH to your Python environment
PATH=/path/to/python/environment

# Send scheduled reminders every day at 12:00 and 00:00
0 12,0 * * * /path/to/python scheduled_reminders.py
```
