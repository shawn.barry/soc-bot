#!/usr/bin/env python3
"""
Defines a log_manager class for centralized logging in an application. 
Supports log rotation, encoding, and outputs to both files and streams. 
Implemented as a singleton to ensure only one instance is created and used.
"""

import logging
from logging.handlers import RotatingFileHandler

class LogManager:
    """
    A singleton class providing application-wide logging functionality.
    Manages log rotation, formatting, and supports multiple handlers.
    """
    # Static variable to hold the singleton instance
    _singleton_instance = None

    def __new__(cls, *args, **kwargs):
        """
        Overrides __new__ to ensure only one instance of log_manager is created.
        Ensures singleton pattern is maintained.
        """
        if not cls._singleton_instance:
            cls._singleton_instance = super(LogManager, cls).__new__(cls)
        return cls._singleton_instance

    def __init__(self, log_file, level=logging.INFO, max_bytes=10485760, backup_count=5):
        """Initializes the log manager with a rotating file handler."""
        # Check if 'log_manager' attribute already exists to avoid re-initialization
        if not hasattr(self, 'log_manager'):
            self.log_manager = logging.getLogger('socbot_manager')
            self.set_level(level)  # Set the logging level

            # Check if a RotatingFileHandler is already added to avoid duplicates
            if not any(isinstance(h, RotatingFileHandler) for h in self.log_manager.handlers):
                handler = RotatingFileHandler(
                    log_file, maxBytes=max_bytes, backupCount=backup_count, encoding='utf-8'
                )
                formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
                handler.setFormatter(formatter)  # Set the log format
                self.log_manager.addHandler(handler)  # Add the handler to the log_manager

    def log(self, level, msg, *args):
        """Logs a message at the specified level."""
        self.log_manager.log(level, msg, *args)

    def set_level(self, level):
        """Sets the logging level for the logger."""
        self.log_manager.setLevel(level)

    def add_stream_handler(self, level=logging.ERROR):
        """
        Adds a stream handler to output logs to stderr.
        """
        # Check if a StreamHandler is already added to avoid duplicates
        if not any(isinstance(h, logging.StreamHandler) for h in self.log_manager.handlers):
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(level)  # Set the level for the stream handler
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            stream_handler.setFormatter(formatter)  # Set the log format
            self.log_manager.addHandler(stream_handler)  # Add the handler to the log_manager


# Example usage of the LogManager class
if __name__ == "__main__":
    # Initialize the LogManager with a log file and settings for log rotation
    LOG_FILE = 'example.log'
    log_manager = LogManager(LOG_FILE, level=logging.DEBUG, max_bytes=10000, backup_count=3)

    # Add a stream handler to also output logs to stderr
    log_manager.add_stream_handler(level=logging.WARNING)

    # Log messages at various levels
    log_manager.log(logging.DEBUG, "This is a debug message.")
    log_manager.log(logging.INFO, "This is an informational message.")
    log_manager.log(logging.WARNING, "This is a warning message.")
    log_manager.log(logging.ERROR, "This is an error message.")
    log_manager.log(logging.CRITICAL, "This is a critical message.")
