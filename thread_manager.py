#!/usr/bin/env python3
"""
This module provides thread management functionalities for a Slackbot. It handles 
the creation, retrieval, logging, and termination of threads associated with user interactions.
"""

import time
import logging
import os
from dotenv import load_dotenv
from db_manager import DatabaseManager
from log_manager import LogManager

class ThreadManager:
    """
    Manages user threads for the Slackbot, including fetching, initializing, logging, 
    and terminating threads.
    """

    def __init__(self):
        """
        Initializes the ThreadManager with a database manager and log file.
        """
        load_dotenv()
        log_file = os.getenv("LOG_FILE")
        self.db_manager = DatabaseManager._singleton_instance
        self.log_manager = LogManager(log_file)

    async def fetch_user_thread(self, user):
        """
        Retrieves the thread ID associated with a user or returns None if not found.
        """
        self.log_manager.log(logging.INFO, f"Fetching user thread for user: {user}")
        query = "SELECT thread_id FROM user_threads WHERE username = ?"
        result, _ = self.db_manager.run_query(query, (user,), query_type='fetch')
        return result[0][0] if result else None

    async def initialize_user_thread(self, client, user):
        """
        Creates a new thread for a user and logs the thread ID.
        """
        self.log_manager.log(logging.INFO, f"Creating a new thread for user: {user}")
        new_thread = client.beta.threads.create()
        new_thread_id = new_thread.id
        self.log_user_thread(user, new_thread_id)
        self.log_manager.log(logging.INFO, f"Created new thread ID: {new_thread_id}")
        return new_thread_id

    def log_user_thread(self, user, thread_id):
        """
        Logs the thread ID associated with a user in the database.
        """
        self.log_manager.log(logging.INFO, f"Logging thread ID: {thread_id} for user: {user}")
        query = '''
            INSERT INTO user_threads (username, thread_id) 
            VALUES (?, ?) 
            ON CONFLICT(username) 
            DO UPDATE SET thread_id = excluded.thread_id
        '''
        self.db_manager.run_query(query, (user, thread_id), commit=True)
        self.log_manager.log(logging.INFO, "User thread logged")


    def terminate_active_threads(self, client, thread_id):
        """
        Terminates all active threads associated with a given thread ID.
        """
        self.log_manager.log(logging.INFO, f"Terminating active threads for thread ID: {thread_id}")
        active_runs = client.beta.threads.runs.list(thread_id=thread_id)
        for run in active_runs.data:
            client.beta.threads.runs.cancel(run_id=run.id)
            self.log_manager.log(logging.INFO, f"Cancelled active run: {run}")
            time.sleep(1)
        self.log_manager.log(logging.INFO, "Active threads terminated")
        return True
