#!/usr/bin/env python3
"""
A Slackbot handler script that interfaces with ChatGPT, processes user input,
handles commands, and manages responses. It includes rate limiting, security analysis,
user feedback processing, and humor integration in responses. The script uses various
components like a database manager for record-keeping, a rate limiter for managing request
frequency, and user input analyzers for understanding user intents.
"""

import random
from ipaddress import ip_address, AddressValueError
import logging
import os
import requests
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from dotenv import load_dotenv
from db_manager import DatabaseManager
from log_manager import LogManager
from chatgpt_connector import ChatGPTInteractionManager
from thread_manager import ThreadManager

class SlackMessenger:
    """
    Manages the sending of messages to Slack, optionally including GIFs from Giphy 
    and links for more information.
    """

    def __init__(self, slack_token, channel_id, giphy_api_key=None, more_info_link=None):
        """
        Initializes the SlackMessenger with Slack credentials, Giphy API key, and 
        an optional link for more information.
        """
        self.slack_client = WebClient(token=slack_token)
        self.channel_id = channel_id
        self.giphy_api_key = giphy_api_key
        self.more_info_link = more_info_link

    def send_to_slack(self, persona, message):
        """
        Sends a rich message to Slack, including a persona, text, optional Giphy GIF, 
        and a more info link.
        """
        try:
            giphy_url = self._get_giphy_url(persona) if self.giphy_api_key else None
            blocks = self._format_message_blocks(persona, message, giphy_url)
            response = self.slack_client.chat_postMessage(channel=self.channel_id, blocks=blocks)
            return response
        except SlackApiError as slack_error:
            print(f"Error sending message to Slack: {slack_error}")
            return None

    def send_simple_message(self, message):
        """
        Sends a simple text message to Slack.
        """
        try:
            response = self.slack_client.chat_postMessage(channel=self.channel_id, text=message)
            return response
        except SlackApiError as slack_error:
            print(f"Error sending simple message to Slack: {slack_error}")
            return None

    def _get_giphy_url(self, query):
        """
        Retrieves a Giphy URL based on the provided query.
        """
        try:
            base_url = "https://api.giphy.com/v1/gifs/search"
            search_params = f"?api_key={self.giphy_api_key}&q={query}&rating=g&limit=5"
            response = requests.get(f"{base_url}{search_params}", timeout=30)
            response.raise_for_status()
            data = response.json()
            return random.choice(
                data['data']
            )['images']['fixed_height_small']['url'] if data['data'] else None
        except requests.RequestException as req_error:
            print(f"Error fetching Giphy URL: {req_error}")
            return None

    def _format_message_blocks(self, persona, message, giphy_url):
        """
        Formats the message blocks for Slack, incorporating an image and text.
        """
        blocks = [
            {"type": "divider"},
            {"type": "image", "image_url": giphy_url, "alt_text": persona},
            {"type": "section", "text": {"type": "mrkdwn", "text": f"*{persona}*: _{message}_"}}
        ]
        if self.more_info_link:
            blocks += self._add_more_info_button()
        return blocks

    def _add_more_info_button(self):
        """
        Adds a 'More Info' button to the message blocks.
        """
        return [
            {
                "type": "actions", 
                "elements": [
                    {
                        "type": "button", 
                        "text": {"type": "plain_text", "text": "More Info"},
                        "url": self.more_info_link, 
                        "action_id": "button_more_info"
                    }
                ]
            },
            {"type": "divider"}
        ]

class SlackbotHandler:
    """
    Handles interactions with the Slackbot. Manages initialization, input processing, 
    command execution, and intent-based response handling.
    """

    def __init__(self):
        """
        Initializes the Slackbot handler, setting up the necessary components 
        like database, rate limiter, and ChatGPT API manager.
        """
        load_dotenv()
        log_file = os.getenv("LOG_FILE")
        self.db_manager = DatabaseManager._singleton_instance
        self.chatgpt_api = ChatGPTInteractionManager()
        self.log_manager = LogManager(log_file)
        self.thread_manager = ThreadManager()
        self.request_text = None
        self.request_user = None
        self.log_manager.log(logging.INFO, "Initialized SlackbotHandler")

    async def process_user_input(self, request):
        """
        Processes the user input, handling commands, checking rate limits, 
        and generating intent-based responses.
        """
        self.request_text = getattr(request, 'text', None)
        self.request_user = getattr(request, 'user', None)

        if self.request_text is None or self.request_user is None:
            missing_attributes = []
            if self.request_text is None:
                missing_attributes.append('text')
            if self.request_user is None:
                missing_attributes.append('user')
            error_message = f"Request object does not have the following attribute(s): " \
                            f"{', '.join(missing_attributes)}."
            self.log_manager.log(logging.ERROR, error_message)
            return {"response": f"Error: Request format is incorrect. Missing attribute(s): " \
                                f"{', '.join(missing_attributes)}."}

        self.log_manager.log(logging.INFO, f"Processing user input from user: {self.request_user}")

        response = await self.handle_commands()
        if not response:
            thread_id = await self.thread_manager.fetch_user_thread(self.request_user)
            if not thread_id:
                thread_id = await self.thread_manager.initialize_user_thread(self.chatgpt_api.client, self.request_user)
            response_dict = await self.chatgpt_api.query_with_user_message(self.request_text, thread_id=thread_id)
            response = response_dict.get("response", "")

        return {"response": response}

    async def handle_commands(self):
        """
        Identifies and processes specific commands ('check', 'report') within the user's input.
        """
        command, *args = self.request_text.split()
        if command.lower() in ['check', 'report'] and args:
            return await self.check_ip_address(command, args[0])

    async def check_ip_address(self, command, ip_arg):
        """
        Executes specific commands like 'check' or 'report' based on the user's request.
        """
        try:
            ip_obj = ip_address(ip_arg)
            if not ip_obj.is_global:
                return {"response": "Please provide a valid global IP address."}
        except AddressValueError:
            return {"response": "Invalid IP address format. Please provide a valid IP address."}

        self.log_manager.log(logging.INFO, f"Executing command: {command} with IP: {ip_arg}")

        # Logic for executing specific commands...
        return {"response": ip_obj}
