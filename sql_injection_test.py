import requests

def send_requests():
    users = ["user1", "user2", "user3", "user4", "user5",
             "user6", "user7", "user8", "user9", "user10"]
    url = "http://127.0.0.1:8000/slack-message"
    total_requests = 1

    count = 0
    for _ in range(total_requests):
        for user in users:
            count += 1
            data = {
                "user": user,
                "text": "Hello"
            }
            response = requests.post(url, json=data, timeout=300)
            print(response.text)  # Print response for readability

            if count >= total_requests:
                return

if __name__ == "__main__":
    send_requests()
